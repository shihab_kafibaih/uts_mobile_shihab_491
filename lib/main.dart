import 'package:flutter/material.dart';
import './control.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'UTS MOBILE APP',
      home: Scaffold(
        backgroundColor: Colors.blue[100],
        appBar: AppBar(
          backgroundColor: Colors.blue[400],
          leading: new Icon(Icons.image),
          title: Text('UTS MOBILE SHIHAB'),
          actions: <Widget>[
            // new Icon(Icons.more_horiz),
          ],
        ),
        body: Control(),
      ),
    );
  }
}
