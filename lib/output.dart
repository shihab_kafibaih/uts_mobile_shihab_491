import 'package:flutter/material.dart';

class Output extends StatelessWidget {
  final String nama;
  Output(this.nama);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: Text(
          '$nama',
          style: new TextStyle(fontSize: 30.0),
        ),
      ),
    );
  }
}
