import 'package:flutter/material.dart';
import './output.dart';

class Control extends StatefulWidget {
  @override
  _ControlState createState() => _ControlState();
}

class _ControlState extends State<Control> {
  String msg = 'Muhammad Shihab';

  void _changeText() {
    setState(() {
      if (msg.startsWith('M')) {
        msg = 'Shihab';
      } else if (msg.startsWith('S')) {
        msg = 'Kafibaih';
      } else {
        msg = 'Muhammad Shihab';
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Image.asset('img/wpap.png', width: 200.0,),
          Text(
            'Namaku adalah : ',
            style: new TextStyle(fontSize: 25.0),
          ),
          Output(msg),
          RaisedButton(
            child: Text(
              "Ubah Nama",
              style: new TextStyle(color: Colors.white),
            ),
            color: Colors.blue,
            onPressed: _changeText,
          ),
        ],
      ),
    );
  }
}
